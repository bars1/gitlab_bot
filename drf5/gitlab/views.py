import json
import datetime

from django.views.decorators.csrf import csrf_exempt
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view
from rest_framework.generics import GenericAPIView
import requests
import jwt

from drf import settings
from gitlab.models import Project, Users, Merges
from gitlab.serializers import ProjectGetSerializer, ProjectPostSerializer, UsersGetSerializer, \
    UsersSetTimeSerializer

from rest_framework.response import Response


requests.get("https://api.telegram.org/bot"+str(settings.TELEGRAM_TOKEN)+"/setWebhook?url=https://"+str(settings.ALLOWED_HOSTS[1])+"/api/v1/tg_hook/")

header = {"content-type":"application/json"}
data = {"id":"1","merge_requests_events":True,"url":f"https://{settings.ALLOWED_HOSTS[1]}/api/v1/git_hook/"}
for projects in Project.objects.all():
    url = f"{settings.GIT_LAB_API_BASE_URL}{projects.project_id}/hooks?access_token={projects.token}"
    get = requests.get(url)
    j = json.loads(get.text)
    need_hook = True
    for hook in j:
        if hook != 'message':
            if hook['url'] == f"https://{settings.ALLOWED_HOSTS[1]}/api/v1/git_hook/":
                need_hook = False
    if need_hook:
        r = requests.post(url, params=data, headers=header)
        print(True)
    print(need_hook)


class RegistrProject(GenericAPIView):
    '''
    get:Получение проектов админа
    post:регистрация нового пректа
    '''
    serializer_class = ProjectPostSerializer
    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message":"You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])

        proj = Project.objects.filter(author=payload_decoded['user_id'])
        serializer = ProjectGetSerializer(proj, many=True)
        return Response(serializer.data)

    def post(self,request):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])

        review = ProjectPostSerializer(data=request.data)
        if (review.is_valid()):
            try:
                creator_id = requests.get(
                    f"{settings.GIT_LAB_API_BASE_URL}"
                    f"{review.data.get('project_id')}"
                    f"?access_token={review.data.get('token')}").json()["creator_id"]

                users = requests.get(f"{settings.GIT_LAB_API_BASE_URL}"
                                     f"{review.data.get('project_id')}"
                                     f"/users?access_token={review.data.get('token')}").json()

                proj = Project(project_id=review.data.get("project_id"),
                               author=payload_decoded['user_id'],
                               token=review.data.get("token"))
                proj.save()

                persons = []
                for user in users:
                    if user["id"] != creator_id:
                        persons.append(user)
                for list in persons:
                    git_users = Users(gitlab_name=list["name"],status=True,work_time=None)
                    git_users.save()
                    git_users.project.add(proj)
            except KeyError:
                return Response({"error":"incorrect data"})
                pass

        proj = Project.objects.filter(author=payload_decoded['user_id'])
        serializer = ProjectGetSerializer(proj, many=True)

        header = {"content-type":"application/json"}
        data = {"id":"1","merge_requests_events":True,"url":f"https://{settings.ALLOWED_HOSTS[1]}/api/v1/git_hook/"}
        url = f"{settings.GIT_LAB_API_BASE_URL}{review.data.get('project_id')}/hooks?access_token={review.data.get('token')}"
        get = requests.get(url)
        j = json.loads(get.text)
        need = True
        if len(j) == 0:
            r = requests.post(url,params=data,headers=header)
        else:
            for actions in j:
                if actions['merge_requests_events'] == True:
                    need = False
        if need:
            r = requests.post(url, params=data, headers=header)

        return Response(serializer.data)


class GetUsers(GenericAPIView):
    '''
    get:Список всех пользователей
    '''
    def get(self,request):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass

        users = Users.objects.all()
        serializer = UsersGetSerializer(users, many=True)
        return Response(serializer.data)


class UsersChange(GenericAPIView):
    '''
    put:Установить выходной проверяющему
    delete:Удалить пользователя
    '''
    serializer_class = UsersSetTimeSerializer
    def put(self, request, id):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass

        Users.objects.filter(id=id).update(work_time=request.data.get('work_time'))
        users = Users.objects.all()
        serializer = UsersGetSerializer(users, many=True)
        return Response(serializer.data)

    def delete(self,request,id):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass

        Users.objects.filter(id=id).delete()
        users = Users.objects.all()
        serializer = UsersGetSerializer(users, many=True)
        return Response(serializer.data)


def check_permition(token):
    try:
        jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        result = True
    except:
        result = False
    return result

def check_holidays(project_id):
    time = datetime.date.today()
    project = Project.objects.filter(project_id=project_id).get()
    for user in Users.objects.filter(project=project):
        if user.status == False and user.work_time <= time:
            Users.objects.filter(id=user.id).update(status=True,work_time=None)


@swagger_auto_schema(method="get", auto_schema=None)
@api_view(["GET","POST"])
@csrf_exempt
def getMerge(request):
    j = json.loads(request.body)
    username = j["user"]["name"]
    project_id = j["project"]["id"]
    merge_id = j['object_attributes']['iid'] # merge id
    action = j['object_attributes']['action'] # unapproved,reopen,open
    url = j['object_attributes']['url']
    check_holidays(project_id)
    if action == "open":
        user = Project.objects.filter(project_id=project_id)[0].users_set.filter(status=True).exclude(gitlab_name=username)[0]
        Merges(author_gitlab=username,reviewer_gitlab=user.gitlab_name,merge_id=merge_id,project=project_id).save()
        Users.objects.filter(gitlab_name=user.gitlab_name).update(status=False)
        send_telegram(user.telegram_id,"Проверте пожалуйста этот Merge Request \n"+str(url))
    if action == "approved":
        merge = Merges.objects.get(merge_id = merge_id)
        if username == merge.reviewer_gitlab:
            merge.update(status=False)
            Users.objects.get(gitlab_name= username).update(status=True)
    if action == "merge":
        merge = Merges.objects.get(merge_id=merge_id)
        merge.update(status=False)
        Users.objects.get(gitlab_name=merge.reviewer_gitlab).update(status=True)

def send_telegram(user_tg_id,text):
    token = settings.TELEGRAM_TOKEN

    url = "https://api.telegram.org/bot"
    user_id = user_tg_id
    url += token
    method = url + "/sendMessage"

    r = requests.post(method, data={
         "chat_id": user_id,
         "text": text
          })

    if r.status_code != 200:
        raise Exception("post_text error")

@csrf_exempt
@swagger_auto_schema(method="post", auto_schema=None)
@api_view(('POST',))
def getTelegram(request):
    j = json.loads(request.body)
    id = j['message']['from']['id']
    message = j['message']['text']
    if not message.startswith("#"):
        send_telegram(id,"Введите своё имя в GitLab (формат - #Имя)")
    elif message.startswith("#"):
        name = message.split("#")[1]
        if Users.objects.filter(gitlab_name=name).exists():
            Users.objects.filter(gitlab_name=name).update(telegram_id=id,status=True)
            send_telegram(id, "Успешно добавлено")
        else:
            send_telegram(id, "Имя не существует,попробуйте ещё раз")
    return Response({"":""})
