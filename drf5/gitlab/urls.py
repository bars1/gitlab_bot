from django.urls import path
from gitlab import views
from gitlab.views import getMerge,getTelegram


urlpatterns = [
    path("projects/", views.RegistrProject.as_view(), name = 'registration'),
    path("git_hook/", getMerge),
    path("tg_hook/", getTelegram),
    path("users/", views.GetUsers.as_view(), name = 'users'),
    path("users/<str:id>/", views.UsersChange.as_view(), name = 'usersChange'),
]