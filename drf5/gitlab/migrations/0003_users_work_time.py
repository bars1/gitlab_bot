# Generated by Django 3.2 on 2021-05-18 14:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gitlab', '0002_remove_users_work_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='work_time',
            field=models.DateField(auto_now=True),
        ),
    ]
