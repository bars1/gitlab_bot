from datetime import datetime

from django.db import models


class Project(models.Model):
    project_id = models.CharField(max_length=100, primary_key=True)
    author = models.CharField(max_length=100,default="")
    token = models.CharField(max_length=100,null=True,default="")

class Users(models.Model):
    gitlab_name = models.CharField(max_length=100,null=True,default="")
    telegram_id = models.CharField(max_length=100,null=True,default="")
    status = models.BooleanField(default=True)
    project = models.ManyToManyField(Project)
    work_time = models.DateField(auto_now=False, auto_now_add=False,null=True)

class Merges(models.Model):
    author_gitlab = models.CharField(max_length=100)
    reviewer_gitlab = models.CharField(max_length=100)
    project = models.CharField(max_length=100)
    merge_id = models.CharField(max_length=100)
    status = models.BooleanField(default=True)
